type UndefinedIfIsUnionUndefined<T> = undefined extends T ? undefined : never;

type ArrayItemType<A> = A extends Array<infer T> ? T : never;

type ArrayItemChildType<T, Child extends string> = T extends Array<
    infer ArrayItem
>
    ? Child extends ''
        ? ArrayItem
        : Resolve<ArrayItem, Child>
    : never;

/**
 * Returns type of most bottom item on array.
 * - ar[1].b.c.d[0] -> returns type of d[0]
 * - ar[1][0].b     -> returns type of b
 * - [0]            -> returns type of [0]
 *
 * Returns never type if root type is not array.
 * - obj.ect[1]     -> returns never
 * */
type ArrayType<T, Path> =
    Path extends `${infer Parent}[${infer _}]${infer Tail}`
        ? // If Path contains array, there are two patterns - Path starts with '[' or not
          Parent extends ''
            ? ArrayItemChildType<T, Tail>
            : Parent extends keyof T
            ?
                  | (Tail extends `.${infer Child}`
                        ? ArrayItemChildType<Required<T>[Parent], Child>
                        : Tail extends `[${infer _}`
                        ? ArrayType<ArrayItemType<Required<T>[Parent]>, Tail>
                        : Tail extends ''
                        ? ArrayItemType<Required<T>[Parent]>
                        : T[Parent])
                  | UndefinedIfIsUnionUndefined<T[Parent]>
            : never
        : never;

type Resolve<T, Path extends string> = Path extends keyof T
    ? T[Path]
    : Path extends `${infer Parent}.${infer Child}`
    ? Parent extends keyof T
        ?
              | Resolve<Required<T>[Parent], Child>
              | UndefinedIfIsUnionUndefined<T[Parent]>
        : ArrayType<T, Path>
    : ArrayType<T, Path>;

export type PathToType<T, Path extends string, Default = never> = Resolve<
    T,
    Path
> extends never
    ? Default
    : Resolve<T, Path>;

export type SetValueByPath<T> = <P extends string>(
    path: P,
    value: Resolve<T, P>
) => void;

export type GetValueByPath<T> = <P extends string>(path: P) => Resolve<T, P>;

export type ParamTypeIfFunction<T> = T extends (...args: infer P) => any
    ? P
    : never;

export type ReturnTypeIfFunction<T> =
    | (T extends (...args: any) => infer R ? R : never)
    | UndefinedIfIsUnionUndefined<T>;
