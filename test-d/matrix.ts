import { expectError, expectType } from 'tsd';
import { SetValueByPath } from '..';

/* Test for SetValueByPath */
const setArrayWithType: SetValueByPath<number[]> = (path, value) => {};
const setMatrix2WithType: SetValueByPath<number[][]> = (path, value) => {};
const setMatrix3WithType: SetValueByPath<number[][][]> = (path, value) => {};

// valid
expectType<void>(setArrayWithType('[0]', 1));
expectType<void>(setMatrix2WithType('[0][0]', 1));
expectType<void>(setMatrix2WithType('[0]', [1]));
expectType<void>(setMatrix3WithType('[0][0][0]', 1));
expectType<void>(setMatrix3WithType('[0][0]', [1]));
expectType<void>(setMatrix3WithType('[0]', [[1]]));

expectError(setArrayWithType('0', 1));
expectError(setArrayWithType('[0]', [1]));
expectError(setMatrix2WithType('[0][0]', [1]));
expectError(setMatrix2WithType('[0]', 1));
expectError(setMatrix3WithType('[0]', 1));
expectError(setMatrix3WithType('[0][0]', 1));
expectError(setMatrix3WithType('[0][0][0]', [1]));
