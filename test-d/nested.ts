import { expectError, expectType } from 'tsd';
import { PathToType, SetValueByPath } from '..';

type TestObject = {
    number: number;
    union: string | null;
    nest1: {
        nest2: {
            nest3: string;
        };
    };
    opt?: {
        a: number;
        b?: string;
        c: { deep: { nested: string } };
        d?: { matrix: number[] };
    };
    ar: Array<{
        arString: string;
        arOpt?: number;
        arOptAr?: Array<{
            arOptArString: string;
            arOptArOptAr?: string[];
        }>;
    }>;
    matrix2: number[][];
    matrix3: number[][][];
};

/* Test for SetValueByPath */
const setWithType: SetValueByPath<TestObject> = (path, value) => {
    // call formik function
};

// Simple child types
expectType<number>({} as PathToType<TestObject, 'number'>);
expectType<string | null>({} as PathToType<TestObject, 'union'>);

// invalid path
expectType<never>({} as PathToType<TestObject, 'invalid'>);

// nested path
expectType<{ nest2: { nest3: string } }>({} as PathToType<TestObject, 'nest1'>);
expectType<{ nest3: string }>({} as PathToType<TestObject, 'nest1.nest2'>);
expectType<string>({} as PathToType<TestObject, 'nest1.nest2.nest3'>);

// optional
expectType<
    | {
          a: number;
          b?: string;
          c: { deep: { nested: string } };
          d?: { matrix: number[] };
      }
    | undefined
>({} as PathToType<TestObject, 'opt'>);
expectType<number | undefined>({} as PathToType<TestObject, 'opt.a'>);
expectType<string | undefined>({} as PathToType<TestObject, 'opt.b'>);
expectType<string | undefined>(
    {} as PathToType<TestObject, 'opt.c.deep.nested'>
);

// array
expectType<{
    arString: string;
    arOpt?: number;
    arOptAr?: Array<{
        arOptArString: string;
        arOptArOptAr?: string[];
    }>;
}>({} as PathToType<TestObject, 'ar[0]'>);
expectType<string>({} as PathToType<TestObject, 'ar[0].arString'>);
expectType<number | undefined>({} as PathToType<TestObject, 'ar[0].arOpt'>);
expectType<
    | Array<{
          arOptArString: string;
          arOptArOptAr?: string[];
      }>
    | undefined
>({} as PathToType<TestObject, 'ar[0].arOptAr'>);
expectType<
    | {
          arOptArString: string;
          arOptArOptAr?: string[];
      }
    | undefined
>({} as PathToType<TestObject, 'ar[0].arOptAr[1]'>);
expectType<string | undefined>(
    {} as PathToType<TestObject, 'ar[0].arOptAr[1].arOptArOptAr[2]'>
);

// valid
expectType<void>(setWithType('number', 1));
expectType<void>(setWithType('union', null));
expectType<void>(setWithType('ar', [{ arString: 'a', arOpt: 0 }]));
expectType<void>(setWithType('ar[0]', { arString: 'a', arOpt: 0 }));
expectType<void>(setWithType('ar[1].arOptAr', [{ arOptArString: 'x' }]));
expectType<void>(setWithType('ar[1].arOptAr[0]', { arOptArString: 'x' }));
expectType<void>(setWithType('ar[1].arOptAr[1].arOptArString', 'x'));
expectType<void>(setWithType('ar[1].arOptAr[1].arOptArOptAr[0]', 'x'));
expectType<void>(setWithType('ar[2].arString', 'arString'));
expectType<void>(
    setWithType('opt', {
        a: 0,
        c: { deep: { nested: 'deep' } },
        d: { matrix: [0] },
    })
);
expectType<void>(setWithType('opt.a', 0));
expectType<void>(setWithType('opt.b', undefined));
expectType<void>(setWithType('opt.c.deep.nested', 'hi!'));
expectType<void>(setWithType('opt.d', { matrix: [0] }));
expectType<void>(setWithType('opt.d.matrix', [0]));
expectType<void>(setWithType('opt.d.matrix[0]', 0));

// invalid
expectError(setWithType('invalid', 1));
expectError(setWithType('arOpt', 'invalid'));
expectError(setWithType('ar', [{ arString: null, arOpt: 'invalid' }]));
expectError(setWithType('ar[2].arString', 999));
expectError(setWithType('matrix2[1][0]', 'invalid'));
expectError(setWithType('matrix3[1][0][0]', 'invalid'));
expectError(setWithType('obj.d', { matrix: ['b'] }));
expectError(setWithType('obj.d.matrix', ['b']));
expectError(setWithType('obj.d.matrix[0]', 'b'));

// valid
expectType<void>(setWithType('matrix2', [[1], [2]]));
expectType<void>(setWithType('matrix2[1]', [2]));
expectType<void>(setWithType('matrix2[1][0]', 2));
expectType<void>(
    setWithType('matrix3', [
        [[1], [2]],
        [[1], [2]],
    ])
);
expectType<void>(setWithType('matrix3[1]', [[1], [2]]));
expectType<void>(setWithType('matrix3[1][0][0]', 2));
