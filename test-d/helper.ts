import { expectType } from 'tsd';
import {
    ParamTypeIfFunction,
    ReturnTypeIfFunction,
    UndefinedIfIsUnionUndefined,
} from '..';

expectType<never>({} as unknown as UndefinedIfIsUnionUndefined<number>);
expectType<undefined>(
    {} as unknown as UndefinedIfIsUnionUndefined<number | undefined>
);
expectType<undefined>({} as unknown as UndefinedIfIsUnionUndefined<undefined>);

expectType<[a: number]>({} as ParamTypeIfFunction<(a: number) => void>);
expectType<[a: number]>(
    {} as ParamTypeIfFunction<((a: number) => void) | undefined>
);
expectType<never>({} as ParamTypeIfFunction<number>);

expectType<string>({} as ReturnTypeIfFunction<(a: number) => string>);
expectType<string | undefined>(
    {} as ReturnTypeIfFunction<((a: number) => string) | undefined>
);
expectType<never>({} as ReturnTypeIfFunction<string>);
