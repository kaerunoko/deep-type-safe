import { PathToType } from '..';

// nested object
type prop = PathToType<{ a: number }, 'a'>; // type prop = number
type child = PathToType<{ a: { b: number } }, 'a'>; // type child = { b: number; }
type nested = PathToType<{ a: { b: number } }, 'a.b'>; // type nested = number

// array type
type array = PathToType<string[], '[0]'>; // type array = string
type matrix = PathToType<string[][], '[1][2]'>; // type matrix = string

// optional
type optional = PathToType<
    { nested?: { optional: number } },
    'nested.optional'
>; // type optional = number | undefined
