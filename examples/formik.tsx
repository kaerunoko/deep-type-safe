import * as React from 'react';
import { useFormikContext } from 'formik';
import { PathToType } from '..';

type YourForm = { with: { nested: { num: number; str: string } } };

export const MyInput = () => {
    const { setFieldValue: _setFieldValue } = useFormikContext<YourForm>();

    // Wrap formik with PathToType and get type-safe methods!
    const setFieldValue = <P extends string>(
        field: P,
        value: PathToType<YourForm, P>,
        shouldValidate?: boolean
    ): void => _setFieldValue(field, value, shouldValidate);

    // no error😀
    const onChange = (value: string) => setFieldValue('with.nested.str', value);

    // error🔥: Argument of type 'string' is not assignable to parameter of type 'number'.
    const invalidValue = (value: string) => setFieldValue('with.nested.num', value);
    // error🔥: Argument of type 'string' is not assignable to parameter of type 'never'.
    const invalidPath = (value: string) => setFieldValue('invalid.path', value);

    return <input onChange={(e) => onChange(e.target.value)} />;
};
