import _ from 'lodash';
import { ParamTypeIfFunction, PathToType, ReturnTypeIfFunction } from '..';

// Wrap lodash with PathToType and get type-safe methods!

/* type-safe return value */
const get = <P extends string, T, D = never>(
    object: T,
    path: P,
    defaultValue?: D
): PathToType<T, P, D> => _.get(object, path, defaultValue);

/* type-safe param */
const set = <P extends string, T extends object, V>(
    obj: T,
    path: P,
    value: PathToType<T, P>
): T => _.set(obj, path, value);

/* type-safe function */
const invoke = <T extends object, P extends string, F extends PathToType<T, P>>(
    object: T,
    path: P,
    ...args: ParamTypeIfFunction<F>
): ReturnTypeIfFunction<F> => _.invoke(object, path, ...args);

// Usage
type MyObject = {
    with: {
        nested: {
            property: number;
            func?: (a: number, b: string) => boolean;
        };
    };
};
const obj: MyObject = {
    with: { nested: { property: 1, func: (a, b) => a === b.length } },
};

const nestedValue = get(obj, 'with.nested.property'); // const value: number
const nestedObject = get(obj, 'with.nested'); // const nestedObject: { property: number; func?: ((a: number, b: string) => boolean) | undefined;}
const invalid = get(obj, 'invalid.path'); // const invalid🔥: never
const defaultValue = get(obj, 'invalid.path', 'default value'); // const defaultValue: "default value"

set(obj, 'with.nested.property', 0); // no error😀
set(obj, 'with.nested', { property: 0 }); // no error😀
set(obj, 'with.nested.property', 'invalid'); // error🔥: Argument of type 'string' is not assignable to parameter of type 'number'.

const result = invoke(obj, 'with.nested.func', 1, 'a'); // const result: boolean | undefined
const invalidParam = invoke(obj, 'with.nested.func', 1, 1); // error🔥: Argument of type 'number' is not assignable to parameter of type 'string'.
