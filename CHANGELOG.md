# Change log

This project adheres to [Semantic Versioning](http://semver.org/).

## 0.0.7

### Document

-   update readme

## 0.0.6

### Document

-   update examples and readme

## 0.0.5

### Document

-   update readme
-   fix deps

## 0.0.4

### Document

-   update readme

### Package

-   remove unnecessary export

## 0.0.3

### Fixed

-   returns union of undefined type when path contains optional property

# Links

-   [0.0.7] https://gitlab.com/kaerunoko/deep-type-safe/-/compare/v0.0.6...v0.0.7
-   [0.0.6] https://gitlab.com/kaerunoko/deep-type-safe/-/compare/v0.0.5...v0.0.6
-   [0.0.5] https://gitlab.com/kaerunoko/deep-type-safe/-/compare/v0.0.4...v0.0.5
-   [0.0.4] https://gitlab.com/kaerunoko/deep-type-safe/-/compare/v0.0.3...v0.0.4
-   [0.0.3] https://gitlab.com/kaerunoko/deep-type-safe/-/compare/v0.0.1...v0.0.3
