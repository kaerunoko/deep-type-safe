# Deep type safe

![NPM](https://img.shields.io/npm/dw/deep-type-safe?style=flat-square)
![NPM](https://img.shields.io/npm/v/deep-type-safe?style=flat-square)
![NPM](https://img.shields.io/npm/l/deep-type-safe?style=flat-square)
![Typescript](https://img.shields.io/badge/TypeScript_^4.1-007ACC?style=flat-square&logo=typescript&logoColor=white)

Deep-type-safe is generic types for function using nested path string.

Remove all `any` type from your project and enjoy TypeScript!😁

```typescript
// == with lodash ==
// lodash `get` will return `any` type when path contains dot or bracket
const thisIsAny = _.get({ nested: { path: 0 } }, 'nested.path');
// Wrap lodash function with PathToType and you will get type-safe value!
const thisIsNumber = get({ nested: { path: 0 } }, 'nested.path');
```

## Install

deep-type-safe works on TypeScript^4.1 because it uses [Template Literal Types](https://devblogs.microsoft.com/typescript/announcing-typescript-4-1-beta/#template-literal-types).

```shell
$ npm install deep-type-safe
```

or

```shell
$ yarn add deep-type-safe
```

## Usage

You can use nested path string with type-safe.

[Examples](https://gitlab.com/kaerunoko/deep-type-safe/-/tree/main/examples/)

-   [use with lodash](https://gitlab.com/kaerunoko/deep-type-safe/-/blob/main/examples/lodash.ts)
-   [use with Formik](https://gitlab.com/kaerunoko/deep-type-safe/-/blob/main/examples/formik.tsx)

### Simple Examples

```typescript
// nested object
type prop = PathToType<{ a: number }, 'a'>; // type prop = number
type child = PathToType<{ a: { b: number } }, 'a'>; // type child = { b: number; }
type nested = PathToType<{ a: { b: number } }, 'a.b'>; // type nested = number

// array type
type array = PathToType<string[], '[0]'>; // type array = string
type matrix = PathToType<string[][], '[1][2]'>; // type matrix = string

// optional
type optional = PathToType<
    { nested?: { optional: number } },
    'nested.optional'
>; // type optional = number | undefined
```

### Type-safe Parameter

```typescript
// with Formik
import { useFormikContext } from 'formik';
import { PathToType } from "deep-type-safe";

type YourForm = {
    with: {
        nested: {
            num: number;
            str: string;
        };
    };
};
export const MyInput = () => {
    const { setFieldValue: _setFieldValue } = useFormikContext<YourForm>();

    // Wrap formik with PathToType and get type-safe methods
    const setFieldValue = <P extends string>(
        field: P,
        value: PathToType<YourForm, P>,
        shouldValidate?: boolean
    ): void => _setFieldValue(field, value, shouldValidate);

    // OK😀
    const onChange = (value: string) => setFieldValue('with.nested.str', value);

    // Type error!🔥
    const invalidValue = (value: string) => setFieldValue('with.nested.num', value);
    const invalidPath = (value: string) => setFieldValue('invalid.path', value);
```

### Type-safe return value

```typescript
// with lodash
import _ from 'lodash';
import { PathToType } from 'deep-type-safe';

type YourType = {
    with: {
        nested: {
            property: number;
        };
    };
};
const obj = { with: { nested: { property: 1 } } };

// Wrap lodash with PathToType
const get = <P extends string, T, D = never>(
    object: T,
    path: P,
    defaultValue?: D
): PathToType<T, P, D> => _.get(object, path, defaultValue);

// number😀
const nestedValue = get(obj, 'with.nested.property');

// never🔥
const invalid = get(obj, 'invalid.path');
```
